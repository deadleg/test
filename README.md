# William test

This app setup with a react frontend using create-react-app, and a backend server using expressjs.

Use the following the run the app:

```bash
npm install
npm start

# new console
npm run server
```

The frontend should be accessable on `localhost:3000`, and the backend on `localhost:3001`.