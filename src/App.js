import React, { Component } from 'react';
import { Container, Row, Card, CardTitle, CardSubtitle, Col, CardBody, Button } from 'reactstrap'
import superagent from 'superagent'

const Item = ({ product, buttonLabel, buttonUpdate, children }) =>
  <Row>
    <Col>
      <Card>
        <CardBody>
          <CardTitle>{product.name}</CardTitle>
          <CardSubtitle>${product.price.toFixed(2)}</CardSubtitle>
          {children}
          <Button onClick={e => buttonUpdate(product)}>{buttonLabel}</Button>
        </CardBody>
      </Card>
    </Col>
  </Row>;

const Cart = ({ cart, buttonUpdate }) => {
  var items = cart.map(item => <Item key={item.name} buttonLabel={"Remove"} buttonUpdate={buttonUpdate} product={item}>
    <div>Quantity: {item.count}</div>
    <div>Total: {(item.count * item.price).toFixed(2)}</div>
  </Item>);
  var total = cart.map(item => item.price * item.count).reduce((acc, val) => acc + val, 0);
  return <Col sm="6">
    <div>
      <h1>Cart</h1>
      <strong>Total: </strong>${total.toFixed(2)}
    </div>
    {items}
  </Col >
}

const Products = ({ buttonUpdate, products }) => {
  const productComponents = products.map(product => <Item key={product.name} buttonLabel={"Add to cart"} buttonUpdate={buttonUpdate} product={product} />);
  return <Col sm="6">
    <h1>Products</h1>
    {productComponents}
  </Col>
};

class App extends Component {
  state = {
    cart: [],
    products: []
  };

  componentDidMount = () => {
    superagent.get('/products')
      .end((err, res) => {
        this.setState({ products: res.body })
      })
    superagent.get('/cart')
      .end((err, res) => {
        this.setState({ cart: res.body })
      })
  }

  removeFromCart = product => {
    const { cart } = this.state;
    if (cart.find(item => item.name === product.name)) {
      const newCart = cart.filter(item => item.name !== product.name);
      this.setState({ cart: newCart });
    }
    superagent.delete(`/cart/${product.name}`)
      .end((err, res) => console.log(err, res));
  }

  addToCart = product => {
    const { cart } = this.state;
    if (!cart.find(item => item.name === product.name)) {
      const cartItem = { count: 1, ...product }
      cart.push(cartItem);

      // Would usually mutate cart after success of this call
      superagent.put('/cart')
        .send(cartItem)
        .end((err, res) => console.log(err, res));
    } else {
      const existing = cart.find(item => item.name === product.name)
      existing.count += 1;

      superagent.put('/cart')
        .send(existing)
        .end((err, res) => console.log(err, res));
    }
    this.setState({ cart: cart })
  }

  render() {
    const { cart, products } = this.state;
    return (
      <Container>
        <Row>
          <Products buttonUpdate={this.addToCart} products={products} />
          <Cart buttonUpdate={this.removeFromCart} cart={cart} />
        </Row>
      </Container >
    );
  }
}

export default App;
