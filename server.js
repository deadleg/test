const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser')
const app = express();
const port = 3001;

app.use(morgan('combined'));
app.use(bodyParser.json())

const store = {
    cart: []
}

const products = [
    { "name": "Sledgehammer", "price": 125.75 },
    { "name": "Axe", "price": 190.50 },
    { "name": "Bandsaw", "price": 562.13 },
    { "name": "Chisel", "price": 12.9 },
    { "name": "Hacksaw", "price": 18.45 }
]

app.get('/products', (req, res) => res.json(products))

app.get('/cart', (req, res) => res.json(store.cart))

app.put('/cart', (req, res) => {
    const { cart } = store;
    const product = req.body
    if (!cart.find(item => item.name === product.name)) {
        cart.push({ count: 1, ...product });
    } else {
        const existing = cart.find(item => item.name === product.name)
        existing.count += 1;
    }
    res.sendStatus(200)
})

app.delete('/cart/:product', (req, res) => {
    const product = req.params.product;
    const { cart } = store;
    if (cart.find(item => item.name === product)) {
        const newCart = cart.filter(item => item.name !== product);
        store.cart = newCart;
    }
    res.sendStatus(200)
})

app.listen(port, () => console.log(`Running on port ${port}`))